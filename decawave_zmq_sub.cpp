//
//  Test client for ZMQ & Protobuf communication for Decawave
//  Doosan Bobcat Clinic 2018-2019
//  Jingnan Shi

#include <zmq.hpp>
#include <zmq_addon.hpp>
#include <iostream>
#include <sstream>

#include <decawave_proto/decawave-message.pb.h>
#include <decawave_proto/decawave-message-utils.h>

int printBytes(unsigned char* array, int size) {
    for (int i = 0; i < size; ++i) {
        printf("%02hhx ", array[i]);
    }
    printf("\n");
}

int main (int argc, char *argv[])
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    zmq::context_t context (1);

    //  Socket to talk to server
    std::cout << "Collecting updates from decawave server…\n" << std::endl;
    zmq::socket_t subscriber (context, ZMQ_SUB);
    subscriber.connect("tcp://localhost:5556");

    //  Subscribe to all messages
    subscriber.setsockopt(ZMQ_SUBSCRIBE, decawave_message_utils::DECAWAVE_MESSAGE_TOPIC,decawave_message_utils::DECAWAVE_MESSAGE_TOPIC_LENGTH);

    //  Process 100 updates
    while(1) {
        decawave_message::TagMessage message;
        zmq::message_t update;
        subscriber.recv(&update);

        std::cout << "ZMQ received bytes: " << std::endl;
        printBytes(static_cast<unsigned char*>(update.data()), update.size());

        size_t protobufMessageSize = update.size()-decawave_message_utils::DECAWAVE_MESSAGE_TOPIC_LENGTH;
        auto* protoMessageArray = new unsigned char[protobufMessageSize]();
        memcpy(protoMessageArray, update.data() + decawave_message_utils::DECAWAVE_MESSAGE_TOPIC_LENGTH, protobufMessageSize);

        std::cout << "Protobuf message bytes (removed prefix): " << std::endl;
        printBytes(protoMessageArray, protobufMessageSize);

        message.ParseFromArray(protoMessageArray, protobufMessageSize);
//
//        std::string message_string(static_cast<char*>(update.data()));
//        message.ParseFromString(message_string);
//        message.ParseFromArray(update.data(), update.size());
//
        std::cout     << "x: "<< message.x() << " "
                      << "y: "<< message.y() << " "
                      << "z: "<< message.z() << " "
                      << "qf: "<< message.qualityfactor()
                      << std::endl;
    }

    return 0;
}